{-# LANGUAGE OverloadedStrings #-}
module Data.ICalendar.Parser.Common where

import Control.Applicative
import Data.Char
import Data.Monoid
import Data.Text (Text, pack)
import Data.Attoparsec.Text

contentLine :: Parser (String, [(String,[String])], String)
contentLine = do
  n <- name
  params <- param `sepBy` (char ';')
  skip (== ':')
  val <- value
  return (n,params,val)

alphaDigit :: Parser Char
alphaDigit = satisfy (inClass "A-Za-z0-9")

alphaDigitMinus :: Parser Char
alphaDigitMinus = satisfy (inClass "A-Za-z0-9-")

name :: Parser [Char]
name = ianaToken <|> xName

ianaToken :: Parser [Char]
ianaToken = many1 alphaDigitMinus

xName :: Parser [Char]
xName = do
  skip (== 'X')
  skip (== '-')
  vid <- maybe "" id <$> optional (vendorId <* "-")
  n <- ianaToken
  return ("X-" <> vid <> "-" <> n)

vendorId :: Parser [Char]
vendorId = (<>) <$> count 3 alphaDigit <*> many alphaDigit

param :: Parser (String, [String])
param = do
  pn <- paramName
  skip (== '=')
  pv <- paramValue
  pvs <- paramValue `sepBy` comma
  return (pn, [pv] <> pvs)

paramName :: Parser [Char]
paramName = ianaToken <|> xName

paramValue :: Parser [Char]
paramValue = paramText <|> quotedString

paramText :: Parser [Char]
paramText = many safeChar

value :: Parser [Char]
value = many valueChar

minus :: Parser Char
minus = char '-'

comma :: Parser Char
comma = char ','

dquote :: Parser Char
dquote = char '"'

quotedString :: Parser [Char]
quotedString = dquote *> many qSafeChar <* dquote

-- TODO: NON-US-ASCII := UTF8-2 | UTF8-3 | UTF8-4
qSafeChar :: Parser Char
qSafeChar = satisfy (inClass (map chr ([0x21] <> [0x23..0x7E]))) <|> space

-- TODO: NON-US-ASCII := UTF8-2 | UTF8-3 | UTF8-4
safeChar :: Parser Char
safeChar = satisfy (inClass (map chr ([0x21] <> [0x23..0x2B] <> [0x2D..0x39] <> [0x3C..0x7E]))) <|> space

-- TODO: NON-US-ASCII := UTF8-2 | UTF8-3 | UTF8-4
valueChar :: Parser Char
valueChar = satisfy (inClass (map chr [0x21..0x7E])) <|> space

control :: Parser Char
control = satisfy (inClass (map chr ([0x00..0x08] <> [0x0A..0x1F] <> [0x7F])))
