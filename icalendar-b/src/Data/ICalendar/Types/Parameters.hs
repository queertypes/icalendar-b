module Data.ICalendar.Types.Parameters where

import Data.Text
import Data.Tagged

newtype ParamValue = ParamValue Text
newtype CalAddress = CalAddress Text

data MType_
data SType_
type MainType = Tagged MType_ Text
type SubType = Tagged SType_ Text
newtype MimeType = MimeType (MainType, SubType)

mainType :: Text -> MainType
mainType = Tagged

subType :: Text -> SubType
subType = Tagged


data URI_
type UncheckedURI = Tagged URI_ Text

uri :: Text -> UncheckedURI
uri = Tagged


data Lang_
type UncheckedLanguage = Tagged Lang_ Text

uncheckedLanguage :: Text -> UncheckedLanguage
uncheckedLanguage = Tagged

data Parameter
  = AltRep UncheckedURI
  | CommonName ParamValue
  | CUType CalendarUserType
  | Delegator [CalAddress]
  | Delegatee [CalAddress]
  | DirectoryEntry UncheckedURI
  | EncodingParam Encoding
  | FormatType MimeType
  | FreeBusyTimeType FreeBusy
  | Language UncheckedLanguage
  | Membership [CalAddress]
  | ParticipationStatusE ParticipationEvent
  | ParticipationStatusT ParticipationTodo
  | ParticipationStatusJ ParticipationJournal
  | RecurrenceRange
  | AlarmTrigger
  | RelationshipType RelType
  | Role RoleType
  | RSVPExpectation RSVP
  | SentBy CalAddress
  | TzId
  | ValueTypeP ValueType
  | Other

data ParticipationEvent
  = EAccepted
  | EDeclined
  | ETentative
  | EDelegated

data ParticipationTodo
  = TNeedsAction
  | TAccepted
  | TDeclined
  | TTentative
  | TDelegated
  | TCompleted
  | TInProcess

data ParticipationJournal
  = JNeedsAction
  | JAccepted
  | JDeclined

data CalendarUserType
  = Individual
  | Group
  | Resource
  | Room
  | Unknown
  | OtherCUType String

data Encoding
  = EightBit
  | Base64

data FreeBusy
  = Free
  | Busy
  | BusyUnavailable
  | BusyTentative

data RelType
  = Parent
  | Child
  | Sibling

data RoleType
  = Chair
  | ReqParticiant
  | OptParticipant
  | NonParticipant

data RSVP = Yes | No

data ValueType
  = IBinary
  | IBoolean
  | ICalAddress
  | IDate
  | IDateTime
  | IDuration
  | IFloat
  | IInteger
  | IPeriod
  | IRecur
  | IText
  | ITime
  | IUri
  | IUtcOffset
